/**
 * This code is free software; you can redistribute it and/or modify it.
 */

package sustainablehomes;

import junit.framework.Assert;

import com.turvo.sustainablehomes.core.configurations.ConfigurationBuilder;
import com.turvo.sustainablehomes.core.engines.SustainableHomeSolutionEngine;
import com.turvo.sustainablehomes.domain.houses.House;
import com.turvo.sustainablehomes.domain.houses.StandardHouse;

/**
 * @author Rajesh Kishore
 * @version 1.0
 * @since V1
 */
public class BasicSeedingTest {

  private static SustainableHomeSolutionEngine engine = null;

  /**
   * @throws java.lang.Exception
   */
  @org.testng.annotations.BeforeClass
  public static void setUpBeforeClass() throws Exception {
    engine =
        ConfigurationBuilder.defaultConfigurationBuilder()
            .defaultConfiguration()
            .createSustainableHomeEngine();
  }

  /**
   * @throws java.lang.Exception
   */
  @org.testng.annotations.AfterClass
  public static void tearDownAfterClass() throws Exception {
    engine.alertStopNotification();
  }

  @org.testng.annotations.Test( priority = 1 )
  public void testSingletonHomeSustainableEngineInstance() {

    SustainableHomeSolutionEngine engine2 =
        ConfigurationBuilder.defaultConfigurationBuilder()
            .defaultConfiguration()
            .createSustainableHomeEngine();

    Assert.assertEquals(engine, engine2);
  }

  /**
   * Test method for
   * {@link com.turvo.sustainablehomes.core.engines.SustainableHomeSolutionEngine#registerHouse(com.turvo.sustainablehomes.domain.houses.House)}
   * .
   */
  @org.testng.annotations.Test( priority = 2 )
  public void testRegisterHouse() {

    House house = new StandardHouse("Alfescro", "ChurchStree, Bangalore East");
    final String houseID = engine.registerHouse(house);

    org.testng.Assert.assertNotNull(houseID,
        "House id could not be registered ");

    org.testng.Assert.assertNotSame(houseID, "",
        "House id could not be registered ");

  }

}
