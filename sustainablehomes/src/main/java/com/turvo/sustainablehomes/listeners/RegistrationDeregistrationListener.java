/**
 * This code is free software; you can redistribute it and/or modify it.
 */

package com.turvo.sustainablehomes.listeners;

/**
 * @author Rajesh Kishore
 * @version 1.0
 * @since V1
 */
public interface RegistrationDeregistrationListener<T> {

  public void register(T t);

  public void deRegister(T t);
}
