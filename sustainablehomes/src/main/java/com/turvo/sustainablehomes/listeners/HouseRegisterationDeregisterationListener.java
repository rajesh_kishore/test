/**
 * This code is free software; you can redistribute it and/or modify it.
 */

package com.turvo.sustainablehomes.listeners;

import com.turvo.sustainablehomes.domain.houses.House;

/**
 * @author Rajesh Kishore
 * @version 1.0
 * @since V1
 */
public class HouseRegisterationDeregisterationListener implements
    RegistrationDeregistrationListener<House> {

  /*
   * (non-Javadoc)
   * 
   * @see
   * com.turvo.sustainablehomes.listeners.RegistrationDeregistrationListener#register(java.lang
   * .Object)
   */
  public void register(House t) {
    // TODO Auto-generated method stub

  }

  /*
   * (non-Javadoc)
   * 
   * @see
   * com.turvo.sustainablehomes.listeners.RegistrationDeregistrationListener#deRegister(java.
   * lang.Object)
   */
  public void deRegister(House t) {
    // TODO Auto-generated method stub

  }

}
