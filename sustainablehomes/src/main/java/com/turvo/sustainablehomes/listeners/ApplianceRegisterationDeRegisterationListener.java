/**
 * This code is free software; you can redistribute it and/or modify it.
 */

package com.turvo.sustainablehomes.listeners;

import com.turvo.sustainablehomes.domain.appliances.Appliance;

/**
 * @author Rajesh Kishore
 * @version 1.0
 * @since V1
 */
public final class ApplianceRegisterationDeRegisterationListener implements
    RegistrationDeregistrationListener<Appliance> {

  /*
   * (non-Javadoc)
   * 
   * @see
   * com.turvo.sustainablehomes.listeners.RegistrationDeregistrationListener#register(java.lang
   * .Object)
   */
  public void register(Appliance t) {
    // TODO Auto-generated method stub

  }

  /*
   * (non-Javadoc)
   * 
   * @see
   * com.turvo.sustainablehomes.listeners.RegistrationDeregistrationListener#deRegister(java.
   * lang.Object)
   */
  public void deRegister(Appliance t) {
    // TODO Auto-generated method stub

  }

}
