/**
 * This code is free software; you can redistribute it and/or modify it.
 */

package com.turvo.sustainablehomes.domain.houses;

import java.util.List;

import com.turvo.sustainablehomes.domain.appliances.Appliance;

/**
 * @author Rajesh Kishore
 * @version 1.0
 * @since V1
 */
public abstract class House {

  private String houseID;

  private String houseName;

  private String houseAddress;

  /**
   * Returns the unmodifiable list of appliances.
   * 
   * @return the unmodifiable list of appliances.
   */
  public abstract List<Appliance> associatedAppliances();

  public House( String name, String address ) {
    this.houseName = name;
    this.houseAddress = address;
  }

  public House( String id, String name, String address ) {
    this(name, address);
    this.houseID = id;
  }

  /**
   * @return the houseID
   */
  public String houseID() {
    return houseID;
  }

  /**
   * @return the houseName
   */
  public String houseName() {
    return houseName;
  }

  /**
   * @return the houseAddress
   */
  public String houseAddress() {
    return houseAddress;
  }

  /*
   * (non-Javadoc)
   * 
   * @see java.lang.Object#toString()
   */
  @Override
  public String toString() {
    return "[" + houseName + "," + houseID + "]";
  }
}
