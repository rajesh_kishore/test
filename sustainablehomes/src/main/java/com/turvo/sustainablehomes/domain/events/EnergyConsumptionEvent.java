/**
 * This code is free software; you can redistribute it and/or modify it.
 */

package com.turvo.sustainablehomes.domain.events;

/**
 * @author Rajesh Kishore
 * @version 1.0
 * @since V1
 */
final class EnergyConsumptionEvent implements HomeSolutionEvent {

  /*
   * (non-Javadoc)
   * 
   * @see com.turvo.sustainablehomes.domain.events.HomeSolutionEvent#time()
   */
  @Override
  public long time() {
    // TODO Auto-generated method stub
    return 0;
  }

  /*
   * (non-Javadoc)
   * 
   * @see
   * com.turvo.sustainablehomes.domain.events.HomeSolutionEvent#houseApplianceID
   * ()
   */
  @Override
  public String houseApplianceID() {
    // TODO Auto-generated method stub
    return null;
  }

  /*
   * (non-Javadoc)
   * 
   * @see com.turvo.sustainablehomes.domain.events.HomeSolutionEvent#houseID()
   */
  @Override
  public String houseID() {
    // TODO Auto-generated method stub
    return null;
  }

  /*
   * (non-Javadoc)
   * 
   * @see
   * com.turvo.sustainablehomes.domain.events.HomeSolutionEvent#applianceID()
   */
  @Override
  public String applianceID() {
    // TODO Auto-generated method stub
    return null;
  }

  /*
   * (non-Javadoc)
   * 
   * @see
   * com.turvo.sustainablehomes.domain.events.HomeSolutionEvent#notification()
   */
  @Override
  public String notification() {
    // TODO Auto-generated method stub
    return null;
  }

}
