/**
 * This code is free software; you can redistribute it and/or modify it.
 */

package com.turvo.sustainablehomes.domain.energies;

/**
 * @author Rajesh Kishore
 * @version 1.0
 * @since V1
 */
public final class StandardEnergy extends Energy {

  /**
   * @param id
   * @param name
   */
  public StandardEnergy( String id, String name ) {
    super(id, name, 0);
  }

}
