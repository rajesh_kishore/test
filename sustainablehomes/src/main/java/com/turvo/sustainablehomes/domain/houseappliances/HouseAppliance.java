/**
 * This code is free software; you can redistribute it and/or modify it.
 */

package com.turvo.sustainablehomes.domain.houseappliances;

/**
 * @author Rajesh Kishore
 * @version 1.0
 * @since V1
 */
public final class HouseAppliance {

  private final String id;

  private final String houseID;

  private final String applianceID;

  /**
   * 
   */
  public HouseAppliance( String id, String houseID, String applianceID ) {
    this.id = id;
    this.houseID = houseID;
    this.applianceID = applianceID;
  }

  /*
   * (non-Javadoc)
   * 
   * @see java.lang.Object#toString()
   */
  @Override
  public String toString() {
    return "ID - " + id() + ", houseID - " + houseID() + ", applianceID "
        + applianceID();
  }

  /**
   * @return the id
   */
  public String id() {
    return id;
  }

  /**
   * @return the houseID
   */
  public String houseID() {
    return houseID;
  }

  /**
   * @return the applianceID
   */
  public String applianceID() {
    return applianceID;
  }

}
