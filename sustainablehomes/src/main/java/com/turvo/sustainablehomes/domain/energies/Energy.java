/**
 * This code is free software; you can redistribute it and/or modify it.
 */

package com.turvo.sustainablehomes.domain.energies;

/**
 * @author Rajesh Kishore
 * @version 1.0
 * @since V1
 */
public abstract class Energy {

  private final String id;

  private final String name;

  /**
   * The type of energy - 0 for standard, 1 for discounted and 2 for free
   */
  private final int energyType;

  /**
   * 
   */
  public Energy( String id, String name, int type ) {
    this.id = id;
    this.name = name;
    this.energyType = type;
  }

  public String id() {
    return id;
  }

  public String energyName() {
    return name;
  }

}
