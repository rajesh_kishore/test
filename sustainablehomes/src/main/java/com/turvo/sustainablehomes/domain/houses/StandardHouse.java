/**
 * This code is free software; you can redistribute it and/or modify it.
 */

package com.turvo.sustainablehomes.domain.houses;

import java.util.List;

import com.turvo.sustainablehomes.domain.appliances.Appliance;

/**
 * @author Rajesh Kishore
 * @version 1.0
 * @since V1
 */
public final class StandardHouse extends House {

  /*
   * (non-Javadoc)
   * 
   * @see com.turvo.sustainablehomes.domain.houses.House#associatedAppliances()
   */
  @Override
  public List<Appliance> associatedAppliances() {
    return null;
  }

  public StandardHouse( String name, String address ) {
    super(name, address);
  }

  public StandardHouse( String id, String name, String address ) {
    super(id, name, address);
  }

}
