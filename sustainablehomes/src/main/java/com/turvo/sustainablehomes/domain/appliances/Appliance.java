/**
 * This code is free software; you can redistribute it and/or modify it.
 */

package com.turvo.sustainablehomes.domain.appliances;

import java.util.LinkedList;
import java.util.List;

import com.turvo.sustainablehomes.domain.energies.Energy;

/**
 * @author Rajesh Kishore
 * @version 1.0
 * @since V1
 */
public abstract class Appliance {

  private final String id;

  private final String applianceName;

  private final int energyQtyConsumptionType;

  protected int usageType;

  protected final List<Energy> energyConsumes = new LinkedList<Energy>();

  public Appliance( String id, String applianceName, int energyConsumptionType,
      List<Energy> energyConsumes ) {
    this.id = id;
    this.applianceName = applianceName;
    this.energyQtyConsumptionType = energyConsumptionType;

    energyConsumes.forEach((k) -> this.energyConsumes.add(k));

  }

  protected Appliance() {
    id = null;
    applianceName = null;
    energyQtyConsumptionType = 0;
  }

  /**
   * Returns the unmodifiable list of {@link Energy}.
   * 
   * @return
   */
  public abstract List<Energy> energyConsummes();

  /**
   * @return
   */
  public String id() {
    return this.id;
  }

  /**
   * @return
   */
  public String name() {
    return applianceName;
  }

  public int energyQuantityConsumptionType() {
    return energyQtyConsumptionType;
  }

  public int usageType() {
    return usageType;
  }

}
