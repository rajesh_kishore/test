/**
 * This code is free software; you can redistribute it and/or modify it.
 */

package com.turvo.sustainablehomes.domain.events;

/**
 * @author Rajesh Kishore
 * @version 1.0
 * @since V1
 */
public interface HomeSolutionEvent {

  /**
   * @return The time at which the event has occured.
   */
  public long time();

  /**
   * @return The house appliance id.
   */
  public String houseApplianceID();

  /**
   * @return The house ID.
   */
  public String houseID();

  /**
   * @return The appliance ID.
   */
  public String applianceID();

  /**
   * It contains the actual notification.
   *
   * @return The actual notification.
   */
  public String notification();

}
