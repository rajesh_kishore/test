/**
 * This code is free software; you can redistribute it and/or modify it.
 */

package com.turvo.sustainablehomes.domain.appliances;

import java.util.List;

import com.turvo.sustainablehomes.common.constants.Constant;
import com.turvo.sustainablehomes.core.generators.ApplianceIDGenerator;
import com.turvo.sustainablehomes.core.generators.IDGenerator;
import com.turvo.sustainablehomes.domain.energies.Energy;

/**
 * @author Rajesh Kishore
 * @version 1.0
 * @since V1
 */
final class DailyNeedApplianceFactory {

  public static final DailyNeedApplianceFactory FACTORY =
      new DailyNeedApplianceFactory();

  private static final IDGenerator idGenerator = ApplianceIDGenerator
      .getInstance();

  /**
   * 
   */
  private DailyNeedApplianceFactory() {
  }

  Appliance createAppliance(String applianceName,
      int categoryByConsumption, List<Energy> energyConsumes) {
    if ( categoryByConsumption == Constant.APPLIANCE_STANDARD_ENERGY_CONSUMPTION ) {
      return new DailyNeedAppliance(
          new StandardEnergyConsumptionAppliance(idGenerator.generateID(),
              applianceName, energyConsumes));
    } else if ( categoryByConsumption == Constant.APPLIANCE_HIGH_ENERGY_CONSUMPTION ) {
      return new DailyNeedAppliance(new HighEnergyConsumptionAppliance(
          idGenerator.generateID(),
          applianceName, energyConsumes));
    } else if ( categoryByConsumption == Constant.APPLIANCE_LOW_ENERGY_CONSUMPTION ) {
      return new DailyNeedAppliance(new FreeEnergyConsumptionAppliance(
          idGenerator.generateID(),
          applianceName, energyConsumes));
    }

    return new DailyNeedAppliance(
        new StandardEnergyConsumptionAppliance(
            idGenerator.generateID(),
            applianceName, energyConsumes));
  }

}
