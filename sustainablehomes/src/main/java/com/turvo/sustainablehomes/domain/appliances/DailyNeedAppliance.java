/**
 * This code is free software; you can redistribute it and/or modify it.
 */

package com.turvo.sustainablehomes.domain.appliances;

import java.util.List;

import com.turvo.sustainablehomes.domain.energies.Energy;

/**
 * @author Rajesh Kishore
 * @version 1.0
 * @since V1
 */
final class DailyNeedAppliance extends Appliance {

  private final Appliance appliance;

  /**
   * 
   */
  DailyNeedAppliance( Appliance appliance ) {
    this.appliance = appliance;
    super.usageType = 1;
  }

  /*
   * (non-Javadoc)
   * 
   * @see com.turvo.sustainablehomes.domain.appliances.Appliance#energyConsummes()
   */
  @Override
  public List<Energy> energyConsummes() {
    return appliance.energyConsummes();
  }

  /*
   * (non-Javadoc)
   * 
   * @see com.turvo.sustainablehomes.domain.appliances.Appliance#id()
   */
  @Override
  public String id() {
    return appliance.id();
  }

  /*
   * (non-Javadoc)
   * 
   * @see com.turvo.sustainablehomes.domain.appliances.Appliance#name()
   */
  @Override
  public String name() {
    return appliance.name();
  }

  /*
   * (non-Javadoc)
   * 
   * @see com.turvo.sustainablehomes.domain.appliances.Appliance#usageType()
   */
  @Override
  public int usageType() {
    return super.usageType();
  }
}
