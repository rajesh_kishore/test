/**
 * This code is free software; you can redistribute it and/or modify it.
 */

package com.turvo.sustainablehomes.domain.appliances;

import java.util.List;

import com.turvo.sustainablehomes.common.constants.Constant;
import com.turvo.sustainablehomes.core.generators.ApplianceIDGenerator;
import com.turvo.sustainablehomes.core.generators.IDGenerator;
import com.turvo.sustainablehomes.domain.energies.Energy;

/**
 * @author Rajesh Kishore
 * @version 1.0
 * @since V1
 */
public final class AbstractApplianceFactory {

  public static final AbstractApplianceFactory APPLIANCEFACTORY =
      new AbstractApplianceFactory();

  private static final IDGenerator applianceIDGenerator = ApplianceIDGenerator
      .getInstance();

  /**
   * 
   */
  private AbstractApplianceFactory() {
  }

  public Appliance createAppliance(String applianceName,
      int categoryByConsumption, int categoryByUsage,
      List<Energy> energyConsumes) {

    if ( categoryByUsage == Constant.APPLIANCE_USAGE_DAILY ) {
      return DailyNeedApplianceFactory.FACTORY
          .createAppliance(
              applianceName, categoryByConsumption, energyConsumes);
    } else if ( categoryByUsage == Constant.APPLIANCE_USAGE_LESS ) {
      return LessFrequentNeedApplianceFactory.FACTORY
          .createAppliance(
              applianceName, categoryByConsumption, energyConsumes);
    }

    return DailyNeedApplianceFactory.FACTORY
        .createAppliance(
            applianceName, categoryByConsumption, energyConsumes);
  }

}
