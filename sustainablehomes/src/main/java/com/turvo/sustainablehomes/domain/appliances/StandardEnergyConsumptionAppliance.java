/**
 * This code is free software; you can redistribute it and/or modify it.
 */

package com.turvo.sustainablehomes.domain.appliances;

import java.util.List;

import com.turvo.sustainablehomes.domain.energies.Energy;

/**
 * @author Rajesh Kishore
 * @version 1.0
 * @since V1
 */
public final class StandardEnergyConsumptionAppliance extends Appliance {

  /**
   * 
   */
  public StandardEnergyConsumptionAppliance( String id, String name,
      List<Energy> energyConsumes ) {
    super(id, name, 0, energyConsumes);
  }

  /*
   * (non-Javadoc)
   * 
   * @see com.turvo.sustainablehomes.domain.appliances.Appliance#energyConsummes()
   */
  @Override
  public List<Energy> energyConsummes() {

    return null;
  }

}
