/**
 * This code is free software; you can redistribute it and/or modify it.
 */

package com.turvo.sustainablehomes.core.generators;

/**
 * @author Rajesh Kishore
 * @version 1.0
 * @since V1
 */
public class ApplianceHouseIDGenerator extends IDGenerator {

  private final static String DEFAULT_PREFIX = "HA";

  private IDGenerator idGenerator = new DefaultIDGenerator() {
  };

  private static final IDGenerator applianceHouseIDGenerator =
      new ApplianceHouseIDGenerator();

  /**
   * The default private constructor to avoid explicit instantiation.
   */
  private ApplianceHouseIDGenerator() {
  }

  /*
   * (non-Javadoc)
   * 
   * @see com.turvo.sustainablehomes.core.generators.DefaultIDGenerator#id()
   */
  @Override
  public String generateID() {
    return DEFAULT_PREFIX + idGenerator.generateID();
  }

  public static IDGenerator getInstance() {
    return applianceHouseIDGenerator;
  }

}
