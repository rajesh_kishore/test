/**
 * This code is free software; you can redistribute it and/or modify it.
 */

package com.turvo.sustainablehomes.core.factories;

import com.turvo.sustainablehomes.core.generators.EnergyIDGenerator;
import com.turvo.sustainablehomes.core.generators.IDGenerator;
import com.turvo.sustainablehomes.domain.energies.DiscountedEnergy;
import com.turvo.sustainablehomes.domain.energies.Energy;
import com.turvo.sustainablehomes.domain.energies.FreeEnergy;
import com.turvo.sustainablehomes.domain.energies.StandardEnergy;

/**
 * The factory class used for creating different energy type;
 *
 * @author Rajesh Kishore
 * @version 1.0
 * @since V1
 */
public final class EnergyFactory {

  public static final EnergyFactory ENERGYFACTORY = new EnergyFactory();

  private IDGenerator energyIDGenerator = EnergyIDGenerator.getInstance();

  /**
   * 
   */
  private EnergyFactory() {
  }

  public Energy createEnergy(String energyName, int energyType) {
    if ( energyType == 0 ) {
      return new StandardEnergy(energyIDGenerator.generateID(),
          energyName);
    } else if ( energyType == 1 ) {
      return new DiscountedEnergy(energyIDGenerator.generateID(),
          energyName);
    } else if ( energyType == 2 ) {
      return new FreeEnergy(energyIDGenerator.generateID(),
          energyName);
    }

    return new StandardEnergy(energyIDGenerator.generateID(),
        energyName);
  }
}
