/**
 * This code is free software; you can redistribute it and/or modify it.
 */

package com.turvo.sustainablehomes.core.services;

import com.turvo.sustainablehomes.domain.houses.House;

/**
 * The api class for providing house appliance services.
 *
 * @author Rajesh Kishore
 * @version 1.0
 * @since V1
 */
public interface HouseAndApplianceService {

  /**
   * Registers the house in the system.
   *
   * @param house
   * @return The generated house ID.
   */
  public String registerHouse(House house);

  /* public String registerAppliance(Appliance appliance); */

  /**
   * Associates the appliance and the house in the system.
   *
   * @param applianceID The appliance ID.
   * @param houseID The house ID.
   * @return The registered house appliance id.
   */
  public String associateApplianceWithHouse(String applianceID, String houseID);
}
