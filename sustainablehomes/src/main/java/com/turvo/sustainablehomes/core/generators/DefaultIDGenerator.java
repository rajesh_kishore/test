/**
 * This code is free software; you can redistribute it and/or modify it.
 */

package com.turvo.sustainablehomes.core.generators;

import java.util.concurrent.atomic.AtomicLong;

/**
 * @author Rajesh Kishore
 * @version 1.0
 * @since V1
 */
abstract class DefaultIDGenerator extends IDGenerator {

  // private static final IDGenerator idGenerator = new DefaultIDGenerator();

  protected AtomicLong defaultID = new AtomicLong(0);

  /*
   * (non-Javadoc)
   * 
   * @see com.turvo.sustainablehomes.core.generators.IDGenerator#id()
   */
  @Override
  public String generateID() {
    return String.valueOf(defaultID.getAndIncrement());
  }

}
