/**
 * This code is free software; you can redistribute it and/or modify it.
 */

package com.turvo.sustainablehomes.core.configurations;

import com.turvo.sustainablehomes.extensions.spi.EventProcessingProvider;

/**
 * The class is the builder class for creating configuration object. This is
 * primary consumer facing class. <code>
 * ConfigurationBuilder configurationBuilder =
        ConfigurationBuilder.defaultConfigurationBuilder();
 * or
 * new ConfigurationBuilder(
            ConfigurationBuilder.DEFAULT_EVENT_PROCESSING_SERVICE_PROVIDER);
            
 * </code>
 * 
 * @author Rajesh Kishore
 * @version 1.0
 * @since V1
 */
public final class ConfigurationBuilder {

  /**
   * The default provider class name for {@link EventProcessingProvider}
   */
  public static final String DEFAULT_EVENT_PROCESSING_SERVICE_PROVIDER =
      "com.turvo.sustainablehomes.extensions.spi.impl.defaultprovider"
          + ".DefaultEventProcessingProvider";

  /**
   * The default configuration builder.
   */
  private static final ConfigurationBuilder DEFAULT_CONFIGURATION_BUILDER;

  static {
    DEFAULT_CONFIGURATION_BUILDER =
        new ConfigurationBuilder(DEFAULT_EVENT_PROCESSING_SERVICE_PROVIDER);
  }

  /**
   * The implementor provider class name for {@link EventProcessingProvider}.
   */
  final private String eventProcessingProvider;

  /**
   * The default and only constructor visible to consumer, which expects the
   * implementor provider class name for {@link EventProcessingProvider} ,
   * consumer can use the Configuration with default object using
   * {@link #buildConfiguration()}.
   * 
   * @param eventProcessingProvider The class name for provider.
   */
  public ConfigurationBuilder( String
      eventProcessingProvider ) {
    this.eventProcessingProvider =
        eventProcessingProvider;
  }

  /**
   * @return the eventProcessingProvider
   */
  final String eventProcessingProvider() {
    return eventProcessingProvider;
  }

  /**
   * Returns the default configuration instance.
   * 
   * @return The default {@link Configuration} instance.
   */
  public Configuration defaultConfiguration() {
    return new Configuration(DEFAULT_CONFIGURATION_BUILDER);
  }

  /**
   * Builds the configuration.
   * 
   * @return The {@link Configuration} instance.
   */
  public Configuration buildConfiguration() {
    return new Configuration(this);
  }

  /**
   * The default configuration builder.
   * 
   * @return the defaultConfigurationBuilder
   */
  public static final ConfigurationBuilder defaultConfigurationBuilder() {
    return DEFAULT_CONFIGURATION_BUILDER;
  }
}
