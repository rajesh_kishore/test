/**
 * This code is free software; you can redistribute it and/or modify it.
 */

package com.turvo.sustainablehomes.core.generators;

/**
 * @author Rajesh Kishore
 * @version 1.0
 * @since V1
 */
public abstract class IDGenerator {

  public abstract String generateID();

  public String defaultPrefix() {
    return "";
  }
}
