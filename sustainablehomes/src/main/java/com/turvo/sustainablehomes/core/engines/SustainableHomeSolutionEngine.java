/**
 * This code is free software; you can redistribute it and/or modify it.
 */

package com.turvo.sustainablehomes.core.engines;

import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;
import java.util.concurrent.atomic.AtomicReference;

import com.turvo.sustainablehomes.common.loggers.LoggerAdapter;
import com.turvo.sustainablehomes.core.alerts.StopProcessingAlert;
import com.turvo.sustainablehomes.core.configurations.Configuration;
import com.turvo.sustainablehomes.core.exceptions.ProviderInstantiationException;
import com.turvo.sustainablehomes.core.services.EventProcessingService;
import com.turvo.sustainablehomes.core.services.HouseAndApplianceService;
import com.turvo.sustainablehomes.domain.events.HomeSolutionEvent;
import com.turvo.sustainablehomes.domain.houses.House;
import com.turvo.sustainablehomes.extensions.spi.EventProcessingProvider;

/**
 * @author Rajesh Kishore
 * @version 1.0
 * @since V1
 */
public final class SustainableHomeSolutionEngine {

  private static final AtomicReference<SustainableHomeSolutionEngine> engine =
      new AtomicReference<SustainableHomeSolutionEngine>();

  private final EventProcessingService eventProcessingService;

  private final HouseAndApplianceService houseAndApplianceService =
      HouseAndApplianceServiceImpl.getInstance();

  private static final LoggerAdapter loggerAdapter = LoggerAdapter
      .getInstance();

  private EventProcessingProvider provider;

  private final List<StopProcessingAlert> alertReceivers =
      new CopyOnWriteArrayList<StopProcessingAlert>();

  private SustainableHomeSolutionEngine( Configuration configuration ) {

    try {
      provider = (EventProcessingProvider)
          Class.forName(configuration.eventProcessingServiceProvider())
              .newInstance();
    } catch ( InstantiationException e ) {
      loggerAdapter.logSevere("PROVIDER_CLASS_INSTANTIATION_ERROR", e);
      throw new ProviderInstantiationException(e);
    } catch ( IllegalAccessException e ) {
      loggerAdapter.logSevere("PROVIDER_CLASS_ILLEGAL_ACCESS_ERROR", e);
      throw new ProviderInstantiationException(e);
    } catch ( ClassNotFoundException e ) {
      loggerAdapter.logSevere("PROVIDER_CLASS_NAME_NOT_FOUND", e);
      throw new ProviderInstantiationException(e);
    }
    alertReceivers.add(provider);
    eventProcessingService = provider.eventProcessingService();
  }

  public static SustainableHomeSolutionEngine getInstanceByConfiguration(
      Configuration configuration) {
    if ( engine.get() == null ) {
      engine.compareAndSet(null, new SustainableHomeSolutionEngine(
          configuration));
    }
    return engine.get();
  }

  public void processEvent(HomeSolutionEvent event) {
    eventProcessingService.submitEvent(event);
  }

  public String registerHouse(House house) {
    return houseAndApplianceService.registerHouse(house);
  }

  public String registerHouseAppliance(String houseID, String applianceID) {
    return houseAndApplianceService.associateApplianceWithHouse(applianceID,
        houseID);
  }

  /*
   * public void energyConsumed(String houseID) { // TODO }
   */

  public void alertStopNotification() {
    alertReceivers.forEach(rcv -> rcv.stopAlert());
  }

}
