/**
 * This code is free software; you can redistribute it and/or modify it.
 */

package com.turvo.sustainablehomes.core.services;

import com.turvo.sustainablehomes.core.alerts.StopProcessingAlert;
import com.turvo.sustainablehomes.domain.events.HomeSolutionEvent;

/**
 * The api class for providing the event processing service. The implementation
 * can be anything be it Kafka topic processor or traditional blocking queue and
 * worker thread implementation.
 *
 * @author Rajesh Kishore
 * @version 1.0
 * @since V1
 */
public interface EventProcessingService extends StopProcessingAlert {

  /**
   * Submits the event to the system, fires and forget.
   *
   * @param event The event.
   */
  public void submitEvent(HomeSolutionEvent event);
}
