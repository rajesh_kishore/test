/**
 * This code is free software; you can redistribute it and/or modify it.
 */

package com.turvo.sustainablehomes.core.engines;

import static com.turvo.sustainablehomes.common.constants.Constant.APPLIANCE_CAR;
import static com.turvo.sustainablehomes.common.constants.Constant.APPLIANCE_HIGH_ENERGY_CONSUMPTION;
import static com.turvo.sustainablehomes.common.constants.Constant.APPLIANCE_USAGE_DAILY;
import static com.turvo.sustainablehomes.domain.appliances.AbstractApplianceFactory.APPLIANCEFACTORY;

import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import com.turvo.sustainablehomes.common.constants.Constant;
import com.turvo.sustainablehomes.core.generators.ApplianceHouseIDGenerator;
import com.turvo.sustainablehomes.core.generators.HomeIDGenerator;
import com.turvo.sustainablehomes.core.services.EnergyService;
import com.turvo.sustainablehomes.core.services.HouseAndApplianceService;
import com.turvo.sustainablehomes.domain.appliances.Appliance;
import com.turvo.sustainablehomes.domain.energies.Energy;
import com.turvo.sustainablehomes.domain.houseappliances.HouseAppliance;
import com.turvo.sustainablehomes.domain.houses.House;
import com.turvo.sustainablehomes.domain.houses.StandardHouse;

/**
 * @author Rajesh Kishore
 * @version 1.0
 * @since V1
 */
final class HouseAndApplianceServiceImpl implements
    HouseAndApplianceService {

  private final Map<String, Appliance> registeredApplianceTypes =
      new ConcurrentHashMap<String, Appliance>();

  private final Map<String, House> registeredHouses =
      new ConcurrentHashMap<String, House>();

  private final Map<String, HouseAppliance> applianceHouseAssociation =
      new ConcurrentHashMap<String, HouseAppliance>();

  private final EnergyService energyService = new EnergyServiceImpl();

  private static final HouseAndApplianceService HOUSEAPPLIANCESERVICE =
      new HouseAndApplianceServiceImpl();

  static HouseAndApplianceService getInstance() {
    return HOUSEAPPLIANCESERVICE;
  }

  /**
   * 
   */
  private HouseAndApplianceServiceImpl() {
    intializeAppliances();
  }

  /**
   * 
   */
  private void intializeAppliances() {
    Appliance appliance = null;
    List<Energy> energyConsumes = null;

    energyConsumes = new LinkedList<>();
    energyConsumes.add(energyService.energyByName(Constant.ENERGY_FUEL));
    appliance =
        APPLIANCEFACTORY.createAppliance(APPLIANCE_CAR,
            APPLIANCE_HIGH_ENERGY_CONSUMPTION, APPLIANCE_USAGE_DAILY,
            energyConsumes);
    registeredApplianceTypes.put(appliance.id(), appliance);

    energyConsumes = new LinkedList<>();
    energyConsumes.add(energyService.energyByName(Constant.ENERGY_ELECTRICITY));
    energyConsumes.add(energyService.energyByName(Constant.ENERGY_WATER));

    appliance =
        APPLIANCEFACTORY.createAppliance(Constant.APPLIANCE_WAHSHING_MACHINE,
            Constant.APPLIANCE_STANDARD_ENERGY_CONSUMPTION,
            Constant.APPLIANCE_USAGE_LESS,
            energyConsumes);
    registeredApplianceTypes.put(appliance.id(), appliance);

    energyConsumes = new LinkedList<>();
    energyConsumes.add(energyService.energyByName(Constant.ENERGY_ELECTRICITY));
    appliance =
        APPLIANCEFACTORY.createAppliance(Constant.APPLIANCE_MICROWAVE,
            APPLIANCE_HIGH_ENERGY_CONSUMPTION, APPLIANCE_USAGE_DAILY,
            energyConsumes);
    registeredApplianceTypes.put(appliance.id(), appliance);
  }

  /*
   * (non-Javadoc)
   * 
   * @see
   * com.turvo.sustainablehomes.core.services.HouseAndApplianceService#registerHouse
   * (com.turvo.sustainablehomes .domain.houses.House)
   */
  @Override
  public String registerHouse(House house) {
    House standardHouse =
        new StandardHouse(HomeIDGenerator.getInstance().generateID(),
            house.houseName(), house.houseAddress());
    this.registeredHouses.put(standardHouse.houseID(), standardHouse);
    return standardHouse.houseID();
  }

  /*
   * (non-Javadoc)
   * 
   * @see com.turvo.sustainablehomes.core.services.HouseAndApplianceService#
   * associateApplianceWithHouse (java.lang.String, java.lang.String)
   */
  @Override
  public String associateApplianceWithHouse(String applianceID, String houseID) {
    HouseAppliance houseAppliance =
        new HouseAppliance(
            ApplianceHouseIDGenerator.getInstance().generateID(),
            houseID, applianceID);
    this.applianceHouseAssociation.put(houseAppliance.id(), houseAppliance);
    return houseAppliance.id();
  }
}
