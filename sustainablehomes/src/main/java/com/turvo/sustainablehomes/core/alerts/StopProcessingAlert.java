/**
 * This code is free software; you can redistribute it and/or modify it.
 */

package com.turvo.sustainablehomes.core.alerts;

/**
 * The interface for alert notification.
 * 
 * @author Rajesh Kishore
 * @version 1.0
 * @since V1
 */
public interface StopProcessingAlert {

  public void stopAlert();
}
