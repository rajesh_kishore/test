/**
 * This code is free software; you can redistribute it and/or modify it.
 */

package com.turvo.sustainablehomes.core.engines;

import static com.turvo.sustainablehomes.common.constants.Constant.ENERGY_ELECTRICITY;
import static com.turvo.sustainablehomes.common.constants.Constant.ENERGY_FOSSIL;
import static com.turvo.sustainablehomes.common.constants.Constant.ENERGY_FUEL;
import static com.turvo.sustainablehomes.common.constants.Constant.ENERGY_GAS;
import static com.turvo.sustainablehomes.common.constants.Constant.ENERGY_SOLAR;
import static com.turvo.sustainablehomes.common.constants.Constant.ENERGY_WATER;
import static com.turvo.sustainablehomes.common.constants.Constant.ENERGY_WIND;
import static com.turvo.sustainablehomes.core.factories.EnergyFactory.ENERGYFACTORY;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import com.turvo.sustainablehomes.core.services.EnergyService;
import com.turvo.sustainablehomes.domain.energies.Energy;

/**
 * @author Rajesh Kishore
 * @version 1.0
 * @since V1
 */
final class EnergyServiceImpl implements EnergyService {

  private final Map<String, Energy> mapEnergies =
      new ConcurrentHashMap<String, Energy>();

  /**
   * 
   */
  EnergyServiceImpl() {
    intializeEnergy();
  }

  /**
   * 
   */
  private void intializeEnergy() {
    // in actual implementation, the source can be from DB

    mapEnergies.put(ENERGY_GAS,
        ENERGYFACTORY.createEnergy(ENERGY_GAS, 0));

    mapEnergies.put(ENERGY_WATER,
        ENERGYFACTORY.createEnergy(ENERGY_WATER, 0));

    mapEnergies.put(ENERGY_FUEL,
        ENERGYFACTORY.createEnergy(ENERGY_FUEL, 0));

    mapEnergies.put(ENERGY_ELECTRICITY,
        ENERGYFACTORY.createEnergy(ENERGY_ELECTRICITY, 0));

    mapEnergies.put(ENERGY_FOSSIL,
        ENERGYFACTORY.createEnergy(ENERGY_FOSSIL, 1));

    mapEnergies.put(ENERGY_SOLAR,
        ENERGYFACTORY.createEnergy(ENERGY_SOLAR, 2));

    mapEnergies.put(ENERGY_WIND,
        ENERGYFACTORY.createEnergy(ENERGY_WIND, 2));
  }

  /*
   * (non-Javadoc)
   * 
   * @see
   * com.turvo.sustainablehomes.core.services.EnergyService#energyByName(java
   * .lang.String)
   */
  @Override
  public Energy energyByName(String energyName) {
    return mapEnergies.get(energyName);
  }

}
