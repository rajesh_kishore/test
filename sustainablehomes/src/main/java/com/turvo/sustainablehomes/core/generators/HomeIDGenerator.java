/**
 * This code is free software; you can redistribute it and/or modify it.
 */

package com.turvo.sustainablehomes.core.generators;

/**
 * @author Rajesh Kishore
 * @version 1.0
 * @since V1
 */
public class HomeIDGenerator extends IDGenerator {

  private final static String DEFAULT_PREFIX = "H";

  private IDGenerator idGenerator = new DefaultIDGenerator() {
  };

  private static final IDGenerator homeIDGenerator = new HomeIDGenerator();

  /**
   * The default private constructor to avoid explicit instantiation.
   */
  private HomeIDGenerator() {
  }

  /*
   * (non-Javadoc)
   * 
   * @see com.turvo.sustainablehomes.core.generators.DefaultIDGenerator#id()
   */
  @Override
  public String generateID() {
    return DEFAULT_PREFIX + idGenerator.generateID();
  }

  public static IDGenerator getInstance() {
    return homeIDGenerator;
  }
}
