/**
 * This code is free software; you can redistribute it and/or modify it.
 */

package com.turvo.sustainablehomes.core.services;

import com.turvo.sustainablehomes.domain.energies.Energy;

/**
 * The api for providing energy related services.
 * 
 * @author Rajesh Kishore
 * @version 1.0
 * @since V1
 */
public interface EnergyService {

  /**
   * Returns the {@link Energy} by name.
   * 
   * @param energyName The energy name.
   * @return the {@link Energy} object.
   */
  public Energy energyByName(String energyName);

}
