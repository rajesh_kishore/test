/**
 * This code is free software; you can redistribute it and/or modify it.
 */

package com.turvo.sustainablehomes.core.configurations;

import com.turvo.sustainablehomes.core.engines.SustainableHomeSolutionEngine;
import com.turvo.sustainablehomes.extensions.spi.EventProcessingProvider;

/**
 * The class contains the raw configuration provided by client. <code>
 * Configuration configuration =
          ConfigurationBuilder.defaultConfigurationBuilder().
          buildConfiguration();
   * or
   * new ConfigurationBuilder(
              ConfigurationBuilder.
              DEFAULT_EVENT_PROCESSING_SERVICE_PROVIDER).
              buildConfiguration();
    </code>
 * 
 * @author Rajesh Kishore
 * @version 1.0
 * @since V1
 */
public final class Configuration {

  /**
   * The {@link EventProcessingProvider} implementor class name.Its not resolved
   * at this point of time.
   */
  final private String eventProcessingProvider;

  Configuration( ConfigurationBuilder configuratorBuilder ) {
    this.eventProcessingProvider =
        configuratorBuilder.eventProcessingProvider();
  }

  /**
   * The method creates the {@link SustainableHomeSolutionEngine} instance given
   * configuration passed by the client.
   * 
   * @return The instance of {@link SustainableHomeSolutionEngine}
   */
  public SustainableHomeSolutionEngine createSustainableHomeEngine() {
    return SustainableHomeSolutionEngine.getInstanceByConfiguration(this);
  }

  /**
   * The event processing service provider name.
   * 
   * @return The implementor class name for {@link EventProcessingProvider}.
   */
  public final String eventProcessingServiceProvider() {
    return eventProcessingProvider;
  }

  /*
   * (non-Javadoc)
   * 
   * @see java.lang.Object#toString()
   */
  @Override
  public String toString() {
    final StringBuilder builder = new StringBuilder(50);
    builder.append("[Provider class name :")
        .append(eventProcessingServiceProvider());
    return builder.toString();
  }

}
