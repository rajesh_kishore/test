/**
 * This code is free software; you can redistribute it and/or modify it.
 */

package com.turvo.sustainablehomes.core.generators;

/**
 * @author Rajesh Kishore
 * @version 1.0
 * @since V1
 */
public class ApplianceIDGenerator extends IDGenerator {

  private final static String DEFAULT_PREFIX = "A";

  private IDGenerator idGenerator = new DefaultIDGenerator() {
  };

  private static final IDGenerator applianceIDGenerator =
      new ApplianceIDGenerator();

  /**
   * The default private constructor to avoid explicit instantiation.
   */
  private ApplianceIDGenerator() {
  }

  /*
   * (non-Javadoc)
   * 
   * @see com.turvo.sustainablehomes.core.generators.DefaultIDGenerator#id()
   */
  @Override
  public String generateID() {
    return defaultPrefix() + idGenerator.generateID();
  }

  /*
   * (non-Javadoc)
   * 
   * @see com.turvo.sustainablehomes.core.generators.IDGenerator#defaultPrefix()
   */
  @Override
  public String defaultPrefix() {
    return DEFAULT_PREFIX;
  }

  public static IDGenerator getInstance() {
    return applianceIDGenerator;
  }
}
