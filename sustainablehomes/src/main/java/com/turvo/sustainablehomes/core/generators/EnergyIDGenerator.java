/**
 * This code is free software; you can redistribute it and/or modify it.
 */

package com.turvo.sustainablehomes.core.generators;

/**
 * @author Rajesh Kishore
 * @version 1.0
 * @since V1
 */
public class EnergyIDGenerator extends IDGenerator {

  private final static String DEFAULT_PREFIX = "E";

  private IDGenerator idGenerator = new DefaultIDGenerator() {
  };

  private static final IDGenerator energyIDGenerator =
      new EnergyIDGenerator();

  /**
   * The default private constructor to avoid explicit instantiation.
   */
  private EnergyIDGenerator() {
  }

  /*
   * (non-Javadoc)
   * 
   * @see com.turvo.sustainablehomes.core.generators.DefaultIDGenerator#id()
   */
  @Override
  public String generateID() {
    return defaultPrefix() + idGenerator.generateID();
  }

  /*
   * (non-Javadoc)
   * 
   * @see com.turvo.sustainablehomes.core.generators.IDGenerator#defaultPrefix()
   */
  @Override
  public String defaultPrefix() {
    return DEFAULT_PREFIX;
  }

  public static IDGenerator getInstance() {
    return energyIDGenerator;
  }

}
