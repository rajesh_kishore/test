/**
 * This code is free software; you can redistribute it and/or modify it.
 */

package com.turvo.sustainablehomes.common.constants;

/**
 * @author Rajesh Kishore
 * @version 1.0
 * @since V1
 */
public final class Constant {

  public final static String ENERGY_GAS = "GAS";

  public final static String ENERGY_WATER = "WATER";

  public final static String ENERGY_FUEL = "FUEL";

  public final static String ENERGY_ELECTRICITY = "ELECTRICITY";

  public final static String ENERGY_SOLAR = "SOLAR";

  public final static String ENERGY_WIND = "WIND";

  public final static String ENERGY_FOSSIL = "FOSSIL";

  public final static int APPLIANCE_STANDARD_ENERGY_CONSUMPTION = 0;

  public final static int APPLIANCE_HIGH_ENERGY_CONSUMPTION = 1;

  public final static int APPLIANCE_LOW_ENERGY_CONSUMPTION = 2;

  public final static int APPLIANCE_USAGE_DAILY = 0;

  public final static int APPLIANCE_USAGE_LESS = 1;

  public final static String APPLIANCE_CAR = "CAR";

  public final static String APPLIANCE_WAHSHING_MACHINE = "WASHING MACHINE";

  public final static String APPLIANCE_MICROWAVE = "MICROWAVE";

  public final static String APPLIANCE_CLEANER = "CLEANER";

  public final static String APPLIANCE_IRON = "IRON";

  public enum INTERVAL {day, month
  };

}
