/**
 * This code is free software; you can redistribute it and/or modify it.
 */

package com.turvo.sustainablehomes.extensions.spi.impl.defaultprovider;

import com.turvo.sustainablehomes.core.services.EventProcessingService;
import com.turvo.sustainablehomes.extensions.spi.EventProcessingProvider;

/**
 * @author Rajesh Kishore
 * @version 1.0
 * @since V1
 */
public class DefaultEventProcessingProvider implements EventProcessingProvider {

  private final EventProcessingService service;

  public DefaultEventProcessingProvider() {
    service = new DefaultEventProcessingService();
  }

  /*
   * (non-Javadoc)
   * 
   * @see
   * com.turvo.sustainablehomes.extensions.spi.EventProcessingProvider#eventProcessingService()
   */
  @Override
  public EventProcessingService eventProcessingService() {
    return service;
  }

  /*
   * (non-Javadoc)
   * 
   * @see com.turvo.sustainablehomes.core.alerts.StopProcessingAlert#stopAlert()
   */
  @Override
  public void stopAlert() {
    service.stopAlert();
  }

}
