/**
 * This code is free software; you can redistribute it and/or modify it.
 */

package com.turvo.sustainablehomes.extensions.spi.impl.defaultprovider;

import java.util.LinkedList;
import java.util.List;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.LinkedBlockingQueue;

import com.turvo.sustainablehomes.core.alerts.StopProcessingAlert;
import com.turvo.sustainablehomes.core.services.EventProcessingService;
import com.turvo.sustainablehomes.domain.events.HomeSolutionEvent;

/**
 * The default event processing engine which takes a worker queue and worker
 * thread.
 * 
 * @author Rajesh Kishore
 * @version 1.0
 * @since V1
 */
final class DefaultEventProcessingService implements EventProcessingService {

  private final BlockingQueue<HomeSolutionEvent> queue;

  // private WorkerTaskProcessor

  private final List<StopProcessingAlert> processingTasks =
      new LinkedList<StopProcessingAlert>();

  /**
   * Note its not a public constructor as its not needed to expose.
   */
  DefaultEventProcessingService() {
    // can be based on configuration as well
    queue = new LinkedBlockingQueue<HomeSolutionEvent>(1024);
    ExecutorService threadPool = Executors.newFixedThreadPool(5);
    for ( int i = 0; i < 5; i++ ) {
      final Runnable task = new WorkerTaskProcessor(queue);
      processingTasks.add((StopProcessingAlert) task);
      threadPool.submit(task);
    }
  }

  /*
   * (non-Javadoc)
   * 
   * @see
   * com.turvo.sustainablehomes.core.services.EventProcessingService#submitEvent
   * (com.turvo.sustainablehomes.domain .events.HomeSolutionEvent)
   */
  @Override
  public void submitEvent(HomeSolutionEvent event) {
    try {
      queue.put(event);
    } catch ( InterruptedException e ) {
      // TODO Auto-generated catch block
      e.printStackTrace();
    }
  }

  /*
   * (non-Javadoc)
   * 
   * @see com.turvo.sustainablehomes.core.alerts.StopProcessingAlert#stopAlert()
   */
  @Override
  public void stopAlert() {
    // do some task
    processingTasks.forEach(t -> t.stopAlert());
  }
}
