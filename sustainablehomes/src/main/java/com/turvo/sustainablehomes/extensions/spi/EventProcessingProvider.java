/**
 * This code is free software; you can redistribute it and/or modify it.
 */

package com.turvo.sustainablehomes.extensions.spi;

import com.turvo.sustainablehomes.core.alerts.StopProcessingAlert;
import com.turvo.sustainablehomes.core.services.EventProcessingService;

/**
 * @author Rajesh Kishore
 * @version 1.0
 * @since V1
 */
public interface EventProcessingProvider extends StopProcessingAlert {

  public EventProcessingService eventProcessingService();

}
