/**
 * This code is free software; you can redistribute it and/or modify it.
 */

package com.turvo.sustainablehomes.extensions.spi.impl.defaultprovider;

import java.util.concurrent.BlockingQueue;
import java.util.concurrent.TimeUnit;

import com.turvo.sustainablehomes.core.alerts.StopProcessingAlert;
import com.turvo.sustainablehomes.domain.events.HomeSolutionEvent;

/**
 * @author Rajesh Kishore
 * @version 1.0
 * @since V1
 */
final class WorkerTaskProcessor implements Runnable, StopProcessingAlert {

  private final BlockingQueue<HomeSolutionEvent> eventQueue;

  private volatile boolean stopNotificationReceived = false;

  /**
   * 
   */
  WorkerTaskProcessor( BlockingQueue<HomeSolutionEvent> eventQueue ) {
    this.eventQueue = eventQueue;
  }

  /*
   * (non-Javadoc)
   * 
   * @see java.lang.Runnable#run()
   */
  @Override
  public void run() {
    while ( !stopNotificationReceived ) {

      try {
        HomeSolutionEvent event = eventQueue.poll(3, TimeUnit.SECONDS);
        if ( event != null ) {
          // TDOD - process
        }
      } catch ( InterruptedException e ) {
        // TODO Auto-generated catch block
        e.printStackTrace();
      }

    }

  }

  /*
   * (non-Javadoc)
   * 
   * @see com.turvo.sustainablehomes.core.alerts.StopProcessingAlert#stopAlert()
   */
  @Override
  public void stopAlert() {
    stopNotificationReceived = true;
  }

}
